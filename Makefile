
.PHONY: all clean purge

all:

clean:
	rm -f */Dockerfile_build
	rm -f */Dockerfile_dep_check

purge: clean
	rm -rf */gpg
	rm -rf */repos
	rm -rf */curl.bsh
	rm -rf */deps.txt
	rm -rf */source/common.inc
	rmdir --ignore-fail-on-non-empty */source