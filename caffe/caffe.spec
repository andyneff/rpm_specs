
%bcond_without cudnn

%bcond_with cpuonly

%include %{_sourcedir}/common.inc
Source999:        common.inc
%define real_name caffe
Name: %{real_name}_local
Provides: %{real_name}

%define sha 62ed0d2bd41a730397e718bae4354c9a5a722624

License:      BSD
Group:        Development/Languages/Other
Summary:      Deep learning framework by the BVLC
Version:      2015.6.12
Release:      2%{?dist}
URL:          http://caffe.berkeleyvision.org/
BuildRoot:    %{_tmppath}/%{name}-%{version}-build
Source0:      https://github.com/BVLC/%{real_name}/archive/%{sha}/%{real_name}-%{sha}.tar.gz
BuildRequires: cmake
BuildRequires: boost-devel >= 1.55
BuildRequires: openblas-devel
BuildRequires: opencv-devel
BuildRequires: protobuf-devel, glog-devel, gflags-devel, hdf5-devel
BuildRequires: leveldb-devel, lmdb-devel, snappy-devel
BuildRequires: python-devel
BuildRequires: numpy
%if %{without cpuonly}
Requires: cuda
%if %{with cudnn}
BuildRequires: cudnn-devel
Requires: cudnn
%endif
%endif

Requires: protobuf, glog, gflags, hdf5
Requires: openblas
Requires: boost >= 1.55
Requires: opencv
Requires: leveldb
Requires: lmdb
Requires: snappy
Requires: python, numpy
#Patch0:       falcon_ini_install_location.patch

%description
Caffe is a deep learning framework made with expression, speed, and modularity in mind. It is developed by the Berkeley Vision and Learning Center (BVLC) and community contributors.

Check out the project site for all the details like

-DIY Deep Learning for Vision with Caffe
-Tutorial Documentation
-BVLC reference models and the community model zoo
-Installation instructions

%package python
Provides: %{real_name}-python
Summary: Caffe Python Module
%description python
Caffe for python

%package devel
Provides: %{real_name}-devel
Summary: Caffe Devel
%description devel
Caffe for Development

%package data
Provides: %{real_name}-all
Summary: Caffe data examples
%description data
Caffe data examples

%package examples
Provides: %{real_name}-examples
Summary: Caffe examples
%description examples
Caffe examples

%package python-examples
Provides: %{real_name}-python-examples
Summary: Caffe python examples
%description python-examples
Caffe python examples, mostly notebooks

%package all
Provides: %{real_name}-all
Summary: Install all Caffe packages
Requires: %{real_name}-python
Requires: %{real_name}-devel
Requires: %{real_name}-data
Requires: %{real_name}-examples
Requires: %{real_name}-python-examples
%description all
All virtual pacakges

%prep
%setup -q -n %{real_name}-%{sha}

%build
mkdir build
pushd build

%{__cmake} -DBLAS=open \
           -DOpenBLAS_INCLUDE_DIR=/usr/include/openblas \
           -DCMAKE_BUILD_TYPE=Release \
           -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
%if %{with cpuonly}
           -DCPUONLY=True\
%endif
           ..

make %{?_smp_mflags} DESTDIR="%{buildroot}"
popd

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
mkdir -p "$RPM_BUILD_ROOT%{_datadir}/Caffe"
cp -ra examples "$RPM_BUILD_ROOT%{_datadir}/Caffe"
mkdir -p "$RPM_BUILD_ROOT%{_sharedstatedir}/Caffe"
cp -ra data "$RPM_BUILD_ROOT%{_sharedstatedir}/Caffe"

pushd build
  mkdir -p "$RPM_BUILD_ROOT%{_libdir}"
  mkdir -p "$RPM_BUILD_ROOT%{_bindir}"
  mkdir -p "$RPM_BUILD_ROOT%{_includedir}"
  mkdir -p "$RPM_BUILD_ROOT%{_python_sitelib}"
  make install DESTDIR="%{buildroot}"
  mv %{buildroot}%{cat_prefix}/lib/* %{buildroot}/%{_libdir}/
  mv %{buildroot}%{cat_prefix}/python/* %{buildroot}/%{_python_sitelib}/
  rmdir %{buildroot}%{cat_prefix}/python
popd

find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|TOOLS=.*|TOOLS=%{_bindir}|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|EXAMPLES=.*|EXAMPLES=%{_bindir}|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|BUILD=.*|BUILD=%{_bindir}|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|=examples|=%{_datadir}/Caffe/examples|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|\./examples|%{_datadir}/Caffe/examples|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -i 's|\(EXAMPLE=\).*/\(.*\)|\1%{_datadir}/Caffe/examples/\2|' \{\} \;
find "$RPM_BUILD_ROOT%{_datadir}/Caffe/examples" -name '*.sh' -exec sed -ri 's|=\.?/?data/|=%{_sharedstatedir}/Caffe/data/|' \{\} \;

%preun data
rm -f %{_sharedstatedir}/Caffe/data/*/*ubyte

%files
%{_bindir}/*
%{_libdir}/*.so
%{_datadir}/Caffe/*.cmake

%files devel
%{_includedir}/*
%{_libdir}/*.a

%files python
%{prefix}/%{_python_sitelib}/*

%files examples
%{_datadir}/Caffe/examples/*
%exclude %{_datadir}/Caffe/examples/*.ipynb

%files python-examples
%{_datadir}/Caffe/examples/*.ipynb

%files data
%defattr(666, root, root, 777)
%{_sharedstatedir}/Caffe/data

%changelog
* Wed Dec 2 2015 Andrew Neff <andrew.neff@visionsystemsinc.com> 2015.6.12-2
- Fixed missing numpy dependency

* Mon Nov 30 2015 Andrew Neff <andrew.neff@visionsystemsinc.com> 2015.6.12-1
- Initial Version
