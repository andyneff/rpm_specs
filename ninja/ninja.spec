%include %{_sourcedir}/common.inc
Source999:        common.inc
%define real_name ninja
Name: %{real_name}_local
Provides: %{real_name}
License:      GPLv3
Group:        Development/Languages/Other
Summary:      A small build system closest in spirit to Make 
Version:      1.6.0
Release:      1%{?dist}
URL:          https://github.com/martine/ninja
BuildRoot:    %{_tmppath}/%{name}-%{version}-build
Source0:      https://github.com/ninja-build/%{real_name}/archive/v%{version}/%{real_name}-%{version}.tar.gz
BuildRequires: re2c
#Patch0:       falcon_ini_install_location.patch

%description
Ninja is yet another build system. It takes as input the interdependencies of files (typically source code and output executables) and orchestrates building them, quickly.

%prep
%setup -q -n %{real_name}-%{version}

export CXXFLAGS="$RPM_OPT_FLAGS"
export CFLAGS="$RPM_OPT_FLAGS"

%build
export CXXFLAGS="$RPM_OPT_FLAGS"
export CFLAGS="$RPM_OPT_FLAGS"

./configure.py --bootstrap

%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mv ninja $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/ninja
%doc COPYING HACKING.md

%changelog
* Thu Nov 19 2015 andrew.neff@visionsystemsinc.com
- Updates to docker rpm build version
* Wed Sep 2 2015 andrew.neff@visionsystemsinc.com
- Updated to 1.6.0
* Sat Feb 12 2011 matt@milliams.com
- Fix source tarball name
- Add docs to package
* Fri Feb 11 2011 matt@milliams.com
- Initial commit of Ninja
