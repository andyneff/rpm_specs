%include %{_sourcedir}/common.inc
Source999:        common.inc
%define real_name cudnn
Name:         %{real_name}_local
Provides:     %{real_name}
Source0:       http://developer.download.nvidia.com/assets/cuda/secure/cuDNN/v3/cudnn-7.0-linux-x64-v3.0-prod.tgz
#Source:       http://developer.download.nvidia.com/assets/cuda/secure/cuDNN/v2/cudnn-6.5-linux-x64-v2.tgz
#Source:       http://developer.download.nvidia.com/assets/cuda/secure/cuDNN/R1/cudnn-6.5-linux-R1.tgz
NoSource:     0
License:      NVIDIA License
Group:        Development/Languages/Other
Summary:      DNN primitives from nVIDIA
Version:      3.0
Release:      1%{?dist}
URL:          https://developer.nvidia.com/rdp/cudnn-download
Requires:     cuda

%description

NVIDIA cuDNN is a GPU-accelerated library of primitives for deep neural networks

%package devel
Provides: %{real_name}-devel
Summary: cuDNN Development Package
%description devel

Development package for cuDNN

%prep
%setup -q -n cuda

%build

%install
mkdir -p $RPM_BUILD_ROOT%{cuda_libdir}/
mkdir -p $RPM_BUILD_ROOT%{cuda_includedir}/
install -p -m 755 lib64/libcudnn.so.7.0.64 $RPM_BUILD_ROOT%{cuda_libdir}/
install -p -m 644 lib64/libcudnn_static.a $RPM_BUILD_ROOT%{cuda_libdir}/
install -p -m 644 include/cudnn.h $RPM_BUILD_ROOT%{cuda_includedir}/

ln -s libcudnn.so.7.0.64 $RPM_BUILD_ROOT%{cuda_libdir}/libcudnn.so.7.0
ln -s libcudnn.so.7.0 $RPM_BUILD_ROOT%{cuda_libdir}/libcudnn.so

%files
%defattr(-, root, root, -)
%{cuda_libdir}/libcudnn.so.*

%files devel
%defattr(-, root, root, -)
%{cuda_includedir}/cudnn.h
%{cuda_libdir}/libcudnn_static.a
%{cuda_libdir}/libcudnn.so

%changelog
* Mon Nov 30 2015 Andrew Neff <andrew.neff@visionsystemsinc.com> 3.0-1
- Initial Version
